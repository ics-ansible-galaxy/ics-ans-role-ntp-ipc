ics-ans-role-ntp-ipc
===================

Configure NTP daemon in IPC rootfs.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

- ...

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: ics-ans-role-ntp-ipc }
```

License
-------

BSD 2-clause
